import multipledispatch


@multipledispatch.dispatch(int)
def ad_hog(int_inp: int) -> int:
    return int_inp


@multipledispatch.dispatch(str)
def ad_hog(int_inp: str) -> str:
    return int_inp


int_inp = input("Введите число: ")
# int_inp = int(input("Введите число: "))

print(ad_hog(int_inp) * 3)
